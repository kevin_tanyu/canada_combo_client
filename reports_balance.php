<?php

$usuariosHouse = array(1);
$usuariosAgent = array(2);
$usuariosStore = array(3);

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$dateFrom = date('m/d/Y');
$dateTo = date('m/d/Y');

$userID = $_SESSION['IDUsuario'];


/*GET PAYOUT*/
$sqlHitsTypes = "SELECT * FROM Scoring_Types WHERE active = 1";
$stmtHitsTypes = $pdoConn->prepare($sqlHitsTypes);
$stmtHitsTypes->execute();
$hits = $stmtHitsTypes->fetchAll(PDO::FETCH_ASSOC);

$sqlHitsParam = "SELECT * FROM Scoring_Parameters WHERE scoring_type = ?";
$stmtHitsParam = $pdoConn->prepare($sqlHitsParam);


?>



<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Balance&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="float: right; width: 80px; background-color: #000000;" onclick="window.location='home.php'" class="btn btn-default"><font color="white">Back</font></button></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4">
                <form>
                    <label>FROM</label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">
                    <label>TO</label> <input type="text" value="<?php echo $dateFrom ?>" id="toDate" class="datepicker">

                    <input type="submit" value="Show" class="button" onclick="getBalanceInfo(); return false" />
                </form>

            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Payouts
                    </div>
                    <div id="divPayout">
                        <div class="panel-body" style="font-weight: bold">
                            <table>
                                <?php foreach($hits as $hit):?>
                                    <?php $stmtHitsParam->execute(array($hit['id']));
                                          $payout = $stmtHitsParam->fetch();
                                    ?>
                                    <tr>
                                        <td style="width: 220px"><?php echo $hit['display_name'] . ": " . $payout['pays'] . "x"?></td>
                                        <?php if($hit['id'] == 1):?>
                                            <td rowspan="4">
                                                Any Order<br>
                                                4 Different numbers = 200x <br>
                                                One pair = 400x <br>
                                                Two pairs = 800x <br>
                                                3 same numbers = 1200x
                                            </td>
                                        <?php endif?>
                                    </tr>
                                <?php endforeach?>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->



        </br>
        <div id="divBalance"></div>


    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>




    $('.datepicker').datepicker({

    });

    $('#dp3').datepicker();

    //getBalanceInfo
    function getBalanceInfo(){

        var parametros = {
            "fromDate" : $('#fromDate').val(),
            "toDate" : $('#toDate').val()
        };
        $.ajax({
            data : parametros,
            url: 'reports_balance_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divBalance").html("Loading Data...");
            },
            success: function(response){
                $("#divBalance").html(response);
            }
        });

    }//FIN getBalanceInfo



</script>