<?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

//CAPTURA LOS DATOS DEL POST
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');


$dateFrom = date("Y-m-d 00:01", strtotime($dateFrom));
$dateTo = date("Y-m-d 23:59", strtotime($dateTo));

$today = date('Y-m-d');
if($dateFrom == $today){
    $dateFrom = date('Y-m-d H:i:s');
}
$sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
$stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
$stmtGetLastSorteo->execute(array($dateFrom));
$LastSorteo = $stmtGetLastSorteo->fetch();

if($stmtGetLastSorteo->rowCount() > 0){
    $dateFrom = $LastSorteo['FechayHora'];
}else{

    /*SI NO HAY SORTEO ANTES DE LA FECHA INGRESADA CAPTURA EL SIGUIENTE MAS PROXIMO*/
   /* $sqlGetNextSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora > ?
                     ORDER BY FechayHora ASC LIMIT 1";
    $stmtGetNextSorteo = $pdoConn->prepare($sqlGetNextSorteo);
    $stmtGetNextSorteo->execute(array($dateFrom));
    $NextSorteo = $stmtGetNextSorteo->fetch();
    $dateFrom = $NextSorteo['FechayHora'];*/

}//Fin if else

/*SI LA FECHA EN LA QUE TERMINA ES DIFERENTE A LA DE HOY BUSCA EL SORTEO DE ESE DIA O EL EL ULTIMO A LA FECHA Y HORA DEFINIDA*/
if($dateTo != $today){
    $stmtGetLastSorteo->execute(array($dateTo));
    $LastSorteo = $stmtGetLastSorteo->fetch();


    if($stmtGetLastSorteo->rowCount() > 0){
        $dateTo = $LastSorteo['FechayHora'];
    }
}
/*SI LA FECHA DE LOS DOS SORTEOS COINCIDEN LA HORA DE LA FECHA EN LA QUE TERMINA SE SETEA A LAS 23:59 DE ESE DIA */
if($dateFrom == $dateTo){
    $dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');
    $dateTo = date("Y-m-d 23:59", strtotime($dateTo));
}

$userID = $_SESSION['IDUsuario'];



try{


/*******GET VENTA********/
    $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                        FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                        WHERE P.usuarioID = " . $userID ."
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                         ORDER BY P.usuarioID";

    /*******GET PAYMENTS********/
    $sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                           FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                           WHERE PTP.pay_by = " . $userID ."
                           AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                           ORDER BY PTP.pay_by";



    $stmtGetSales = $pdoConn->prepare($sqlGetSales);
    $stmtGetSales->execute();
    $ticketSales = $stmtGetSales->fetchAll(PDO::FETCH_ASSOC);

    $stmtGetPayment = $pdoConn->prepare($sqlGetPayments);
    $stmtGetPayment->execute();
    $ticketPayment = $stmtGetPayment->fetchAll(PDO::FETCH_ASSOC);

    $totalBalance = 0;
    $salesTotal = 0;
    $paymentTotal = 0;



}catch(Exception $e){
    echo('ERROR');
}

?>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <h2 style="color: green"><i class="glyphicon glyphicon-save"></i>    Sales: <label id="lblSales"></label></h2>
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <h2 style="color: red"><i class="glyphicon glyphicon-open"></i>    Payments: <label id="lblPayment"></label></h2>
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <h2> <label id="lblBalance"></label></h2>
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading" style="font-weight: bold">
                Sales Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-sales">
                        <thead>
                        <tr>
                            <th>Seller</th>
                            <th>Date</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($ticketSales as $sale):?>
                            <tr class="gradeA">
                                <td><?php echo $sale['NombreUsuario']?></td>
                                <td><?php echo system_date_format($sale['created_at'])?></td>
                                <td><?php echo system_number_money_format($sale['total'])?></td>
                                <?php $salesTotal = $salesTotal + $sale['total'];?>
                            </tr>
                        <?php endforeach?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading" style="font-weight: bold">
                Payments Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-payment">
                        <thead>
                        <tr>
                            <th>Pay By</th>
                            <th>Ticket Number</th>
                            <th>Amount</th>
                            <th>Pay At</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($ticketPayment as $payment):?>
                            <tr class="gradeA">
                                <td><?php echo $payment['NombreUsuario']?></td>
                                <td><?php echo $payment['ticketID']?></td>
                                <td><?php echo system_number_money_format($payment['prize'])?></td>
                                <td><?php echo system_date_format($payment['pay_at'])?></td>
                                <?php $paymentTotal = $paymentTotal + $payment['prize'];?>
                            </tr>
                        <?php endforeach?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->



<script>
    $(document).ready(function() {
        $('#dataTables-sales').DataTable({
            responsive: true
        });
        $('#dataTables-payment').DataTable({
            responsive: true
        });

        if(<?php echo $salesTotal - $paymentTotal?> >= 0){
            $('#lblBalance').html("<span style='color : green'><i class='fa fa-money'></i>    Balance: <?php echo system_number_money_format($salesTotal - $paymentTotal)?></span>");//;
        }else{
            $('#lblBalance').html("<span style='color : red'><i class='fa fa-money'></i>    Balance: <?php echo system_number_money_format($salesTotal - $paymentTotal)?></span>");//;
        }

        $('#lblSales').html("<?php echo system_number_money_format($salesTotal)?>");//;
        $('#lblPayment').html("<?php echo system_number_money_format($paymentTotal)?>");//;


    });
</script>