<?php
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

$nowDateTime = date('Y-m-d H:i:s');

/*GET LAST SORTEO*/
$sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
$stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
$stmtGetLastSorteo->execute(array($nowDateTime));
$LastSorteo = $stmtGetLastSorteo->fetch();

$dateFrom = $LastSorteo['FechayHora'];

/*******GET VENTA********/
$sqlGetSales = "SELECT  SUM(P.total) as 'total'
                FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                WHERE P.usuarioID = " . $_SESSION['IDUsuario'] ."
                AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $nowDateTime ."'";

/*******GET PAYMENTS********/
$sqlGetPayments = "SELECT SUM(PTP.prize) as 'total'
                   FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                   WHERE PTP.pay_by = " . $_SESSION['IDUsuario'] ."
                   AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $nowDateTime ."'";

$stmtGetSales = $pdoConn->prepare($sqlGetSales);
$stmtGetSales->execute();
$ticketSales = $stmtGetSales->fetch();

$stmtGetPayment = $pdoConn->prepare($sqlGetPayments);
$stmtGetPayment->execute();
$ticketPayment = $stmtGetPayment->fetch();

?>

<p style="font-size: 20px">
    Sales:    <?php echo system_number_money_format($ticketSales['total']) ?></br>
    Payments: <?php echo system_number_money_format($ticketPayment['total'])?></br>
    Balance:  <?php echo system_number_money_format($ticketSales['total'] - $ticketPayment['total'])?></br>
</p>