<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 10:40 AM
 */

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$_SESSION['IDUsuario'];

$nowDateTime = date('Y-m-d H:i:s');
$sqlGetDraw = "SELECT * FROM SorteosProgramacion
               WHERE FechayHora > ?";
$stmtGetDraw = $pdoConn->prepare($sqlGetDraw);
//$nowDateTime = date('Y-m-d H:i:s', strtotime($nowDateTime . ' + 10 minutes'));
$stmtGetDraw->execute(array($nowDateTime));
$dailyDraw = $stmtGetDraw->fetch();

/*PARAMETROS DE APUESTAS*/
$sqlLimitParam = "SELECT * FROM Scoring_Limits SL JOIN Scoring_Types ST ON SL.scoring_type = ST.id";
$stmtLimitParam = $pdoConn->prepare($sqlLimitParam);
$stmtLimitParam->execute();
$LimitParam = $stmtLimitParam->fetchAll(PDO::FETCH_ASSOC);

/*PARAMETROS DE TIQUETES*/
$sqlTicketParam = "SELECT * FROM Ticket_Parameters";
$stmtTicketParam = $pdoConn->prepare($sqlTicketParam);
$stmtTicketParam->execute();
$TicketParam = $stmtTicketParam->fetch();

$MinArray = Array();
$MaxArray = Array();

foreach($LimitParam as $limit){
    array_push($MinArray, $limit['mini']);
    array_push($MaxArray, $limit['maxi']);

}//Fin foreach

$TicketMin = $TicketParam['mini'];
$TicketMax = $TicketParam['maxi'];

/*GET LAST SORTEO*/
$sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
$stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
$stmtGetLastSorteo->execute(array($nowDateTime));
$LastSorteo = $stmtGetLastSorteo->fetch();

$dateFrom = $LastSorteo['FechayHora'];

/*******GET VENTA********/
$sqlGetSales = "SELECT  SUM(P.total) as 'total'
                FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                WHERE P.usuarioID = " . $_SESSION['IDUsuario'] ."
                AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $nowDateTime ."'";

/*******GET PAYMENTS********/
$sqlGetPayments = "SELECT SUM(PTP.prize) as 'total'
                   FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                   WHERE PTP.pay_by = " . $_SESSION['IDUsuario'] ."
                   AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $nowDateTime ."'";

$stmtGetSales = $pdoConn->prepare($sqlGetSales);
$stmtGetSales->execute();
$ticketSales = $stmtGetSales->fetch();

$stmtGetPayment = $pdoConn->prepare($sqlGetPayments);
$stmtGetPayment->execute();
$ticketPayment = $stmtGetPayment->fetch();


?>

<!-- Page Content -->
<div id="page-wrapper" onclick="checkDraw(<?php echo $dailyDraw['ID'];?>)">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <a href="javascript:void(0)" onclick="toggleBalance();">BALANCE</a>
                    <a href="reports_balance.php">

                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <span class="pull-right">View Details&nbsp;</span>


                    </a>
                </div>
                <div id="divBalance">
                    <div class="panel-body" >
                        <div id="divBalanceData"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                   Combination Limits
                </div>
                <div id="divBalance">
                    <div class="panel-body" style="font-weight: bold">
                        <?php foreach($LimitParam as $limit):?>
                            <?php echo $limit['display_name'] ." Minimun:" . $limit['mini']  . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;". $limit['display_name'] ." Maximum:" . $limit['maxi']?> </br>
                        <?php endforeach?>
                    </div>
                </div>
            </div>
        </div>
        <p>
        <form style="float: right">

            <label>Ticket Number</label>
            <input class="form-control" placeholder="#" style="width: 300px" onkeypress='return isNumber(event);' id="txtTicketID" >
            <button type="button" id="btnSearch" class="btn btn-primary" style="margin-left: 230px; margin-top: 5px" onclick="getTicketInfo()" data-toggle="modal" data-target="#myModal">Search</button>

        </form></br></br></br></br></br>
        </p>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
            <div class="modal-dialog" style="width: 770px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Ticket Info</h4>
                    </div>
                    <div class="modal-body" style="width: 770px;">
                        <div id="divTicketInfo"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Draw: <?php echo system_date_format($dailyDraw['FechayHora']);?>&nbsp;&nbsp;<label id="lblChanged" style="color: red"></label></h3>
                    </div>
                    <!-- .panel-heading -->
                    <div class="panel-body">
                      <!--  <label>How many tickets?</label>
                        <select id="slcTicketQuantity" name="slcTicketQuantity" onchange="ajustTicket()">
                            <?php for($i = 1; $i <= 10; $i++):?>
                                <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php endfor?>
                        </select>-->
                        </br></br>
                        <!--TICKETS-->
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Combination
                                    </div>
                                    <div class="panel-body">

                                        <center>
                                        <table>
                                            <tr>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input id="txtNumber1" style="width: 80px; height: 80px; font-size: 60px; text-align: center" onkeyup="changeFocus(event, 2); limit(this)" onclick="clear();" onkeypress="return isNumber(event)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td><input id="txtNumber2" style="width: 80px; height: 80px; font-size: 60px; text-align: center" onkeyup="changeFocus(event, 3); limit(this)" onclick="clear();" onkeypress="return isNumber(event)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td><input id="txtNumber3" style="width: 80px; height: 80px; font-size: 60px; text-align: center" onkeyup="changeFocus(event, 4); limit(this)" onclick="clear();" onkeypress="return isNumber(event)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td><input id="txtNumber4" style="width: 80px; height: 80px; font-size: 60px; text-align: center" onkeyup="changeFocus(event, 5); limit(this)" onclick="clear();" onkeypress="return isNumber(event)"></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <label>1st Number</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td><label>2nd Number</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td><label>3rd Number</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td><label>4th Number</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td style="font-size: 20px; text-align: center; font-weight: bold; background-color: #BDBDBD"><input type="checkbox" name="chkMixed" id="chkMixed" value="5">Any Order</td>
                                            </tr>
                                        </table>
                                        </center>
                                    </div>
                                    <div class="panel-footer">
                                        <!--TICKETS-->
                                        <center>
                                            <label style="font-size: 24px;">Choose Amount </label>
                                            <button type="button" class="btn btn-primary btn-lg" value="1" style="font-size: 35px" onclick="addAmount(1)">&nbsp;1&nbsp;</button>&nbsp;&nbsp;&nbsp;
                                            <button type="button" class="btn btn-primary btn-lg" value="2" style="font-size: 35px" onclick="addAmount(2)">&nbsp;2&nbsp;</button>&nbsp;&nbsp;&nbsp;
                                            <button type="button" class="btn btn-primary btn-lg" value="5" style="font-size: 35px" onclick="addAmount(5)">&nbsp;5&nbsp;</button>
                                        </center>
                                    </div>
                                </div>
                                <!-- /.col-lg-6 -->
                                <div class="col-lg-8" style="width: 100%">&nbsp;&nbsp;&nbsp;
                                    <div class="panel-body">
                                    <center>
                                        <label style="font-size: 34px; text-align: center">Amount: </label>
                                        <label id="lblAmount" style="font-size: 34px">0</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="button" class="btn btn-primary btn-lg" value="SUBMIT" onclick="checkMaxBetCombination(<?php echo $dailyDraw['ID'];?>)">
                                        <!--<input type="button" class="btn btn-warning btn-lg" value="CLEAR AMOUNT" onclick="clearAmount()">-->
                                        <input type="button" class="btn btn-warning btn-lg" value="CLEAR" onclick="clearCombination()">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="button" class="btn btn-danger btn-lg" value="CANCEL" onclick="clearAll()" style="padding-top: -10px">
                                        <input type="button" class="btn btn-success btn-lg" value="PRINT" onclick="saveTicket(<?php echo $dailyDraw['ID'];?>)">
                                        </br>
                                    </center>
                                    </div>
                                </div>
                            <br>
                            </div>

                            <!-- /.col-lg-6 -->
                            <div class="col-lg-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Ticket
                                    </div>
                                    <div class="panel-body">
                                        <label style="">Ticket Minimum: <?php echo $TicketMin?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ticket Maximum: <?php echo $TicketMax?></label>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="tbTicketContent">
                                            <thead>
                                            <center>
                                              <th style="text-align: center">1st Number</th>
                                              <th style="text-align: center">2nd Number</th>
                                              <th style="text-align: center">3rd Number</th>
                                              <th style="text-align: center">4th Number</th>
                                              <th style="text-align: center">Type</th>
                                              <th style="text-align: center">Amount</th>
                                              <th  style="text-align: center"></th>
                                            </center>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                                <tr style='font-size: 18px; text-align: center'>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Total</td>
                                                    <td><label id="lblTotalTicket">0</label></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divTickets"></div>
                            <input type="hidden" value="0" id="contaCampos" name="contaCampos">
                            <center>

                            </center>


                        </div>


                    </div>
                    <!-- .panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->


<script>
var toogle = 1;
$( document ).ready(function() {
    $('#divBalance').hide(200);
    toogle = 0;
});

    //PARA COMPROBAR SI EL SORTEO HA COMBIADO
    function checkDraw(sorteoID){
        var parametros = {
            "action" : "Check"
        };
        $.ajax({
            data : parametros,
            url: 'ticket_action.php',
            type: 'post',
            beforeSend: function(){
               // $("#result").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                if(sorteoID != response){
                    $('#lblChanged').text('DRAW HAS CHANGED');
                    location.reload();

                }else if(response == 'ERROR'){
                    alert('CONNECTION LOST');
                }
            }
        });
    }//FIN checkDraw

    function getTicketInfo(){

        var parametros = {
            "TicketID" : $('#txtTicketID').val()
        };
        $.ajax({
            data : parametros,
            url: 'ticket_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divTicketInfo").html(response);
                $('#txtTicketID').val('');
            }
        });

   }

    //ARRAY PARA GUARDAR LAS FILAS QUE CONTIENEN LAS COMBINACIONES VALIDAS
    var posicionesValidas = Array();

    //Limita que no pueda ingresar mas de 1 caracteres
    function limit(element)
    {
        var max_chars = 1;

        if(element.value.length > max_chars) {
            element.value = element.value.substr(0, max_chars);
        }
    }//FIN limit


    //Para ir cambiando el foucus entre las cajas de texto
    function changeFocus(e, position){
        e = (e) ? e : window.event;
        var charCode = (e.which) ? e.which : e.keyCode;
        //SI oprime backspace
        if(charCode == 8){
            if(position > 1){
                     $("#txtNumber" + (position-2)).focus();
            }
         //SI oprime ENTER
        }else if(charCode == 13){
            if(position == 5){
                addToTicket();
            }
        //SI oprime algun numero
        }else{
            if($("#txtNumber" + position).length){
                if ((charCode > 95 && charCode < 106) || (charCode > 47 && charCode < 57)) {
                    $("#txtNumber" + position).focus();
                }else if(charCode == 8){
                    if(position > 1){
                        $("#txtNumber" + (position-2)).focus();
                    }
                }
            }
        }


    }//FIN CAMBIAR FOCUS

        //Verifica si es numero o no
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }//FIN isNumber

    //Ir sumando los montos
    function addAmount(amount){
        var oldAmount = $("#lblAmount").text();
        $("#lblAmount").text(parseInt(oldAmount) + amount);
    }//Fin addAmount

    //Limpia combinacion + monto
    function clearCombination(){
        $("#txtNumber1").val('');
        $("#txtNumber2").val('');
        $("#txtNumber3").val('');
        $("#txtNumber4").val('');
        $("#lblAmount").text(0);
    }//Fin clearCombination

    //Limpia monto
    function clearAmount(){
        $("#lblAmount").text(0);
    }//Fin clearCombination

    //LimpiaTodo
    function clearAll(){
        $("#txtNumber1").val('');
        $("#txtNumber2").val('');
        $("#txtNumber3").val('');
        $("#txtNumber4").val('');
        $("#lblAmount").text(0);
        $("#lblTotalTicket").text(0);

        //CAPTURA EL CONTADOR
        var count = parseInt($("#contaCampos").val());
        var totalComb = 0;
        for(var i = 0; i < count; i++){
            //Comprueba si elemento existe
            if($("#trCombination"+i).length){
                  $("#trCombination" + i).remove();
            }//Fin if
        }//Fin for

        $("#contaCampos").val(0);

    }//Fin clearAll

    //Anadir combinacion al tiquete
    function addToTicket(){
        //PARA 4 NUMEROS
       /* //VALIDA QUE HAYA INGRESADO TODOS LOS NUMEROS
        if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() != '' && $("#txtNumber4").val() != ''){
            //VALIDA QUE HAYA INGRESADO UN MONTO
                    //CAPTURA EL CONTADOR
                    var count = parseInt($("#contaCampos").val());
                    var monto = $("#lblAmount").text();
                    //AGREGA LA NUEVA COLUMNA DE LA COMBINACION
                    $("#tbTicketContent tr:last").before(
                        "<tr style='font-size: 18px; text-align: center' id='trCombination"+ count +"'>"+
                            "<td><label id='lblCombination"+ count +"1'>" + $("#txtNumber1").val() + "</label</td>" +
                            "<td><label id='lblCombination"+ count +"2'>" + $("#txtNumber2").val() + "</label></td>" +
                            "<td><label id='lblCombination"+ count +"3'>" + $("#txtNumber3").val() + "</label></td>" +
                            "<td><label id='lblCombination"+ count +"4'>" + $("#txtNumber4").val() + "</label></td>" +
                            "<td><label id='lblAmount"+ count +"'>" + monto +"</label></td>" +
                            "<td><button type='button' class='btn btn-danger btn-circle' value='Remove' onclick='removeCombination("+ count +")'><i class='fa fa-times'></button></td>"+
                            "</tr>"
                    );
                    $("#contaCampos").val(count+1);
                    var total = parseInt($("#lblTotalTicket").text());
                    $("#lblTotalTicket").text(total + parseInt($("#lblAmount").text()));
                    clearCombination();

        }else{
            alert('Fill empty spaces');
        }*/

        /*PARA 1,2,3 O 4*/
        if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() == '' && $("#txtNumber3").val() == '' && $("#txtNumber4").val() == ''){

            //CAPTURA EL CONTADOR
            var count = parseInt($("#contaCampos").val());
            var monto = $("#lblAmount").text();
            //AGREGA LA NUEVA COLUMNA DE LA COMBINACION
            $("#tbTicketContent tr:last").before(
                "<tr style='font-size: 18px; text-align: center' id='trCombination"+ count +"'>"+
                    "<td><label id='lblCombination"+ count +"1'>" + $("#txtNumber1").val() + "</label</td>" +
                    "<td>-</td>" +
                    "<td>-</td>" +
                    "<td>-</td>" +
                    "<td><label id='lblPick"+ count +"'>" + "Pick 1" + "</label></td>" +
                    "<td><label id='lblAmount"+ count +"'>" + monto +"</label></td>" +
                    "<td><button type='button' class='btn btn-danger btn-circle' value='Remove' onclick='removeCombination("+ count +")'><i class='fa fa-times'></button></td>"+
                    "<input type='hidden' value='1' id='hdfPick"+ count +"'>" +
                    "</tr>"
            );
            $("#contaCampos").val(count+1);
            var total = parseInt($("#lblTotalTicket").text());
            $("#lblTotalTicket").text(total + parseInt($("#lblAmount").text()));
            clearCombination();


        }else if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() == '' && $("#txtNumber4").val() == ''){

            //CAPTURA EL CONTADOR
            var count = parseInt($("#contaCampos").val());
            var monto = $("#lblAmount").text();
            //AGREGA LA NUEVA COLUMNA DE LA COMBINACION
            $("#tbTicketContent tr:last").before(
                "<tr style='font-size: 18px; text-align: center' id='trCombination"+ count +"'>"+
                    "<td><label id='lblCombination"+ count +"1'>" + $("#txtNumber1").val() + "</label</td>" +
                    "<td><label id='lblCombination"+ count +"2'>" + $("#txtNumber2").val() + "</label></td>" +
                    "<td>-</td>" +
                    "<td>-</td>" +
                    "<td><label id='lblPick"+ count +"' value='2' >" + "Pick 2" + "</label></td>" +
                    "<td><label id='lblAmount"+ count +"'>" + monto +"</label></td>" +
                    "<td><button type='button' class='btn btn-danger btn-circle' value='Remove' onclick='removeCombination("+ count +")'><i class='fa fa-times'></button></td>"+
                    "<input type='hidden' value='2' id='hdfPick"+ count +"'>" +
                    "</tr>"
            );
            $("#contaCampos").val(count+1);
            var total = parseInt($("#lblTotalTicket").text());
            $("#lblTotalTicket").text(total + parseInt($("#lblAmount").text()));
            clearCombination();

        }else if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() != '' && $("#txtNumber4").val() == ''){

            //CAPTURA EL CONTADOR
            var count = parseInt($("#contaCampos").val());
            var monto = $("#lblAmount").text();
            //AGREGA LA NUEVA COLUMNA DE LA COMBINACION
            $("#tbTicketContent tr:last").before(
                "<tr style='font-size: 18px; text-align: center' id='trCombination"+ count +"'>"+
                    "<td><label id='lblCombination"+ count +"1'>" + $("#txtNumber1").val() + "</label</td>" +
                    "<td><label id='lblCombination"+ count +"2'>" + $("#txtNumber2").val() + "</label></td>" +
                    "<td><label id='lblCombination"+ count +"3'>" + $("#txtNumber3").val() + "</label></td>" +
                    "<td>-</td>" +
                    "<td><label id='lblPick"+ count +"' value='3' >" + "Pick 3" + "</label></td>" +
                    "<td><label id='lblAmount"+ count +"'>" + monto +"</label></td>" +
                    "<td><button type='button' class='btn btn-danger btn-circle' value='Remove' onclick='removeCombination("+ count +")'><i class='fa fa-times'></button></td>"+
                    "<input type='hidden' value='3' id='hdfPick"+ count +"'>" +
                    "</tr>"
            );
            $("#contaCampos").val(count+1);
            var total = parseInt($("#lblTotalTicket").text());
            $("#lblTotalTicket").text(total + parseInt($("#lblAmount").text()));
            clearCombination();

        }else if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() != '' && $("#txtNumber4").val() != ''){

            //CAPTURA EL CONTADOR
            var count = parseInt($("#contaCampos").val());
            var monto = parseInt($("#lblAmount").text());
            if(document.getElementById('chkMixed').checked){

                //AGREGA LA NUEVA COLUMNA DE LA COMBINACION
                $("#tbTicketContent tr:last").before(
                    "<tr style='font-size: 18px; text-align: center' id='trCombination"+ count +"'>"+
                        "<td><label id='lblCombination"+ count +"1'>" + $("#txtNumber1").val() + "</label</td>" +
                        "<td><label id='lblCombination"+ count +"2'>" + $("#txtNumber2").val() + "</label></td>" +
                        "<td><label id='lblCombination"+ count +"3'>" + $("#txtNumber3").val() + "</label></td>" +
                        "<td><label id='lblCombination"+ count +"4'>" + $("#txtNumber4").val() + "</label></td>" +
                        "<td><label id='lblPick"+ count +"' value='5' >" + "Any Order" + "</label></td>" +
                        "<td><label id='lblAmount"+ count +"'>" + monto +"</label></td>" +
                        "<td><button type='button' class='btn btn-danger btn-circle' value='Remove' onclick='removeCombination("+ count +")'><i class='fa fa-times'></button></td>"+
                        "<input type='hidden' value='5' id='hdfPick"+ count +"'>" +
                        "</tr>"
                );

            }else{

                //AGREGA LA NUEVA COLUMNA DE LA COMBINACION
                $("#tbTicketContent tr:last").before(
                    "<tr style='font-size: 18px; text-align: center' id='trCombination"+ count +"'>"+
                        "<td><label id='lblCombination"+ count +"1'>" + $("#txtNumber1").val() + "</label</td>" +
                        "<td><label id='lblCombination"+ count +"2'>" + $("#txtNumber2").val() + "</label></td>" +
                        "<td><label id='lblCombination"+ count +"3'>" + $("#txtNumber3").val() + "</label></td>" +
                        "<td><label id='lblCombination"+ count +"4'>" + $("#txtNumber4").val() + "</label></td>" +
                        "<td><label id='lblPick"+ count +"' value='4' >" + "Pick 4" + "</label></td>" +
                        "<td><label id='lblAmount"+ count +"'>" + monto +"</label></td>" +
                        "<td><button type='button' class='btn btn-danger btn-circle' value='Remove' onclick='removeCombination("+ count +")'><i class='fa fa-times'></button></td>"+
                        "<input type='hidden' value='4' id='hdfPick"+ count +"'>" +
                        "</tr>"
                );

            }

            $("#contaCampos").val(count+1);
            var total = parseInt($("#lblTotalTicket").text());
            $("#lblTotalTicket").text(total + monto);
            clearCombination();

        }else{
            alert('Add at least one number');
        }


    }//Fin addToTicket

   //Valida que la combinacion ingresada no supere el monto limite
   function checkMaxTicket(){
       //CAPTURA EL MONTO DEL TIQUETE
       var total = parseInt($("#lblTotalTicket").text());

       //CAPTURA EL MONTO QUE SE VA A INGRESAR
       var monto = parseInt($("#lblAmount").text());

       var totalTemp = total + monto;

       if(totalTemp <= <?php echo $TicketMax?> && totalTemp >= <?php echo $TicketMin?>){
          return true;
       }else{
           return false;
       }


    }//Fin checkMaxBetCombination

    //Valida que la combinacion ingresada no supere el monto limite
    function checkMaxBetCombination(sorteoID){
     /*   //VALIDA QUE HAYA INGRESADO TODOS LOS NUMEROS
        if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() != '' && $("#txtNumber4").val() != ''){
            //CAPTURA EL CONTADOR
            var count = parseInt($("#contaCampos").val());
            var totalComb = 0;
            for(var i = 0; i < count; i++){
                //Comprueba si elemento existe
                if($("#trCombination"+i).length){
                    //Valida si combinacion existe
                    if($('#lblCombination' + i + 1).text() == $("#txtNumber1").val() && $('#lblCombination' + i + 2).text() == $("#txtNumber2").val() && $('#lblCombination' + i + 3).text() == $("#txtNumber3").val() && $('#lblCombination' + i + 4).text() == $("#txtNumber4").val()){
                        var comb = parseInt($('#lblAmount' + i).text());
                        totalComb = totalComb + comb;
                    }//Fin if
                }//Fin if
            }//Fin for

            var dif = <?php //echo $Max?> - totalComb;
            // alert($("#lblAmount").text());
            if(parseInt($("#lblAmount").text()) <= dif){

                var parametros = {
                    "action" : "Combination",
                    "SorteoID" : sorteoID,
                    "Numero1" : $("#txtNumber1").val(),
                    "Numero2" : $("#txtNumber2").val(),
                    "Numero3" : $("#txtNumber3").val(),
                    "Numero4" : $("#txtNumber4").val(),
                    "Monto" : parseInt($("#lblAmount").text()) + totalComb
                };
                $.ajax({
                    data : parametros,
                    url: 'ticket_action.php',
                    type: 'post',
                    beforeSend: function(){

                    },
                    success: function(response){
                        if(response == 'TRUE'){
                            addToTicket();
                        }else{
                            alert(response);
                        }
                    }
                });

            }else{
                alert('Amount exceed combination limit');
            }//Fin if/else
        }else{
            alert('Fill empty spaces');
        }*/

        if($("#lblAmount").text() != '0'){
            /*PARA 1,2,3 O 4*/
            if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() == '' && $("#txtNumber3").val() == '' && $("#txtNumber4").val() == ''){

                //CAPTURA EL CONTADOR
                var count = parseInt($("#contaCampos").val());
                var totalComb = 0;
                for(var i = 0; i < count; i++){
                    //Comprueba si elemento existe
                    if($("#trCombination"+i).length){
                        //Valida si combinacion existe
                        if($('#lblCombination' + i + 1).text() == $("#txtNumber1").val() && $("#hdfPick"+i).val() == 1){
                            var comb = parseInt($('#lblAmount' + i).text());
                            totalComb = totalComb + comb;
                        }//Fin if
                    }//Fin if
                }//Fin for

                var dif = <?php echo $MaxArray[0]?> - totalComb;
                // alert($("#lblAmount").text());
                if(parseInt($("#lblAmount").text()) <= dif){
                    var parametros = {
                        "action" : "Combination",
                        "SorteoID" : sorteoID,
                        "Numero1" : $("#txtNumber1").val(),
                        "Pick" : 1,
                        "Monto" : parseInt($("#lblAmount").text()) + totalComb
                    };
                    $.ajax({
                        data : parametros,
                        url: 'ticket_action.php',
                        type: 'post',
                        beforeSend: function(){

                        },
                        success: function(response){
                            if(response == 'TRUE'){
                                addToTicket();
                            }else{
                                alert(response);
                            }
                        }
                    });

                }else{
                    alert('Amount exceed combination limit');
                }//Fin if/else



            }else if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() == '' && $("#txtNumber4").val() == ''){

                //CAPTURA EL CONTADOR
                var count = parseInt($("#contaCampos").val());
                var totalComb = 0;
                for(var i = 0; i < count; i++){
                    //Comprueba si elemento existe
                    if($("#trCombination"+i).length){
                        //Valida si combinacion existe
                        if($('#lblCombination' + i + 1).text() == $("#txtNumber1").val() && $('#lblCombination' + i + 2).text() == $("#txtNumber2").val() && $("#hdfPick"+i).val() == 2){
                            var comb = parseInt($('#lblAmount' + i).text());
                            totalComb = totalComb + comb;
                        }//Fin if
                    }//Fin if
                }//Fin for

                var dif = <?php echo $MaxArray[1]?> - totalComb;
                // alert($("#lblAmount").text());
                if(parseInt($("#lblAmount").text()) <= dif){
                    var parametros = {
                        "action" : "Combination",
                        "SorteoID" : sorteoID,
                        "Numero1" : $("#txtNumber1").val(),
                        "Numero2" : $("#txtNumber2").val(),
                        "Pick" : 2,
                        "Monto" : parseInt($("#lblAmount").text()) + totalComb
                    };
                    $.ajax({
                        data : parametros,
                        url: 'ticket_action.php',
                        type: 'post',
                        beforeSend: function(){

                        },
                        success: function(response){
                            if(response == 'TRUE'){
                                addToTicket();
                            }else{
                                alert(response);
                            }
                        }
                    });

                }else{
                    alert('Amount exceed combination limit');
                }//Fin if/else


            }else if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() != '' && $("#txtNumber4").val() == ''){

                //CAPTURA EL CONTADOR
                var count = parseInt($("#contaCampos").val());
                var totalComb = 0;
                for(var i = 0; i < count; i++){
                    //Comprueba si elemento existe
                    if($("#trCombination"+i).length){
                        //Valida si combinacion existe
                        if($('#lblCombination' + i + 1).text() == $("#txtNumber1").val() && $('#lblCombination' + i + 2).text() == $("#txtNumber2").val() && $('#lblCombination' + i + 3).text() == $("#txtNumber3").val() && $("#hdfPick"+i).val() == 3){
                            var comb = parseInt($('#lblAmount' + i).text());
                            totalComb = totalComb + comb;
                        }//Fin if
                    }//Fin if
                }//Fin for

                var dif = <?php echo $MaxArray[2]?> - totalComb;
                // alert($("#lblAmount").text());
                if(parseInt($("#lblAmount").text()) <= dif){
                    var parametros = {
                        "action" : "Combination",
                        "SorteoID" : sorteoID,
                        "Numero1" : $("#txtNumber1").val(),
                        "Numero2" : $("#txtNumber2").val(),
                        "Numero3" : $("#txtNumber3").val(),
                        "Pick" : 3,
                        "Monto" : parseInt($("#lblAmount").text()) + totalComb
                    };
                    $.ajax({
                        data : parametros,
                        url: 'ticket_action.php',
                        type: 'post',
                        beforeSend: function(){

                        },
                        success: function(response){
                            if(response == 'TRUE'){
                                addToTicket();
                            }else{
                                alert(response);
                            }
                        }
                    });

                }else{
                    alert('Amount exceed combination limit');
                }//Fin if/else


            }else if($("#txtNumber1").val() != ''&& $("#txtNumber2").val() != '' && $("#txtNumber3").val() != '' && $("#txtNumber4").val() != ''){

                //CAPTURA EL CONTADOR
                var count = parseInt($("#contaCampos").val());
                var totalComb = 0;
                for(var i = 0; i < count; i++){
                    //Comprueba si elemento existe
                    if($("#trCombination"+i).length){
                        if(document.getElementById('chkMixed').checked){//ANY ORDER
                            var arrayCombinacion = [$("#txtNumber1").val(), $("#txtNumber2").val(), $("#txtNumber3").val(), $("#txtNumber4").val()];
                            var arrayNumeros = [$('#lblCombination' + i + 1).text(), $('#lblCombination' + i + 2).text(), $('#lblCombination' + i + 3).text(), $('#lblCombination' + i + 4).text()];
                            //Valida si combinacion existe
                            var hits = 0;
                            for(var p = 0; p < arrayCombinacion.length; p++){
                                for(var j = 0; j < arrayNumeros.length; j++){
                                    if(arrayCombinacion[p] == arrayNumeros[j]){
                                        hits++;
                                        arrayNumeros[j] = -1;
                                    }//Fin if
                                }//Fin for2
                            }//Fin for1
                            if(hits == 4){
                                var comb = parseInt($('#lblAmount' + i).text());
                                totalComb = totalComb + comb;
                            }//Fin if
                        }else{
                            //Valida si combinacion existe
                            if($('#lblCombination' + i + 1).text() == $("#txtNumber1").val() && $('#lblCombination' + i + 2).text() == $("#txtNumber2").val() && $('#lblCombination' + i + 3).text() == $("#txtNumber3").val() && $('#lblCombination' + i + 4).text() == $("#txtNumber4").val() && $("#hdfPick"+i).val() == 4){
                                var comb = parseInt($('#lblAmount' + i).text());
                                totalComb = totalComb + comb;
                            }//Fin if
                        }
                    }//Fin if
                }//Fin for

                if(document.getElementById('chkMixed').checked){

                    var dif = <?php echo $MaxArray[4]?> - totalComb;
                    // alert($("#lblAmount").text() + totalComb);
                    if(parseInt($("#lblAmount").text()) <= dif){
                        var parametros = {
                            "action" : "Combination",
                            "SorteoID" : sorteoID,
                            "Numero1" : $("#txtNumber1").val(),
                            "Numero2" : $("#txtNumber2").val(),
                            "Numero3" : $("#txtNumber3").val(),
                            "Numero4" : $("#txtNumber4").val(),
                            "Pick" : 5,
                            "Monto" : parseInt($("#lblAmount").text()) + totalComb
                        };
                        $.ajax({
                            data : parametros,
                            url: 'ticket_action.php',
                            type: 'post',
                            beforeSend: function(){

                            },
                            success: function(response){
                                if(response == 'TRUE'){
                                    addToTicket();
                                }else{
                                    alert(response);
                                }
                            }
                        });

                    }else{
                        alert('Amount exceed combination limit');
                    }//Fin if/else

                }else{

                    var dif = <?php echo $MaxArray[3]?> - totalComb;
                    // alert($("#lblAmount").text() + totalComb);
                    if(parseInt($("#lblAmount").text()) <= dif){
                        var parametros = {
                            "action" : "Combination",
                            "SorteoID" : sorteoID,
                            "Numero1" : $("#txtNumber1").val(),
                            "Numero2" : $("#txtNumber2").val(),
                            "Numero3" : $("#txtNumber3").val(),
                            "Numero4" : $("#txtNumber4").val(),
                            "Pick" : 4,
                            "Monto" : parseInt($("#lblAmount").text()) + totalComb
                        };
                        $.ajax({
                            data : parametros,
                            url: 'ticket_action.php',
                            type: 'post',
                            beforeSend: function(){

                            },
                            success: function(response){
                                if(response == 'TRUE'){
                                    addToTicket();
                                }else{
                                    alert(response);
                                }
                            }
                        });

                    }else{
                        alert('Amount exceed combination limit');
                    }//Fin if/else

                }



            }else{
                alert('Add at least one number');
            }
        }else{
            alert('Add a amount ');
        }









    }//Fin checkMaxBetCombination

    //Remueve la fila deseada de las combinaciones agregadas
    function removeCombination(row){
        var total = parseInt($("#lblTotalTicket").text());
       $("#lblTotalTicket").text(total - parseInt($("#lblAmount"+row).text()));
        $("#trCombination" + row).remove();
    }//Fin removeCombination

    //Para guardat el ticket
    function saveTicket(sorteoID){

        //VALIDA QUE CUMPLA CON EL LIMITE DE TIQUETE
        var total = parseInt($("#lblTotalTicket").text());

        if(total >= <?php echo $TicketMin?> && total <= <?php echo $TicketMax?>){
            //CAPTURA EL CONTADOR
            var count = parseInt($("#contaCampos").val());
            var validCombination = new Array();
            var scoringCombination = new Array();

            for(var i = 0; i < count; i++){
                //Comprueba si elemento existe
                if($("#trCombination"+i).length){
                    var combination = new Array();
                    var pick = $('#hdfPick' + i).val();
                    if(pick == 5){
                        pick = 4;
                    }
                    for(var z = 1; z <= pick; z++){
                        combination.push($('#lblCombination' + i + z).text());
                    }
                    combination.push($('#lblAmount' + i).text());
                    scoringCombination.push($('#hdfPick' + i).val());
                    validCombination.push(combination);
                }//Fin if
            }//Fin for

            if(validCombination.length > 0){
                var parametros = {
                    "action" : "Save",
                    "combinations" : validCombination,
                    "scoring" : scoringCombination,
                    "total" : $("#lblTotalTicket").text(),
                    "SorteoID" : sorteoID
                };
                $.ajax({
                    data : parametros,
                    url: 'ticket_action.php',
                    type: 'post',
                    beforeSend: function(){
                        $("#page-wrapper").block(	{
                            /*
                             Mensaje a desplegar
                             */
                            message: '<h1>Processing...</h1>',
                            /*
                             Estilo para la caja que despliega el mensaje
                             */
                            css: { border: '3px solid #337AB7' }
                        });
                    },
                    success: function(response){
                        if(response != 'ERROR '){
                            var url = "ticket_view.php?ticket_id=" + response;
                            window.open(url, "PRINT", 'width=277px,height=735px,scrollbars=1');
                        }
                        $("#page-wrapper").unblock();
                        location.reload();
                    }
                });

            }else{
                alert('ADD A COMBINATION');
            }
        }else{
            alert('Ticket amount exceeds limit');
        }



    }//FIN saveTicket


    function toggleBalance(){
        if(toogle == 1){
            $('#divBalance').hide(600);
            toogle = 0;
        }else{
            $('#divBalance').show(600);
            getBalance();
            toogle = 1;
        }

    }

    function getBalance(){

    $.ajax({
        url: 'home_action.php',
        type: 'post',
        beforeSend: function(){
            $("#divBalanceData").html("Loading Data...");
        },
        success: function(response){
            $("#divBalanceData").html(response);
        }
    });

    }//Fin getBalance




</script>