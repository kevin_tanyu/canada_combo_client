<?php

session_start();
// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$ticketID = $_GET['ticket_id'];

try{
    
    $sqlGetTicket = "SELECT * FROM Ticket WHERE ID = ?";
    $stmtGetTicket = $pdoConn->prepare($sqlGetTicket);
    $stmtGetTicket->execute(array($ticketID));
    $ticket = $stmtGetTicket->fetch();

    $sqlGetDraw = "SELECT * FROM SorteosProgramacion WHERE ID = ?";
    $stmtGetDraw = $pdoConn->prepare($sqlGetDraw);
    $stmtGetDraw->execute(array($ticket['sorteoID']));
    $sorteo = $stmtGetDraw->fetch();
    
    $sqlTicketBet = "SELECT * FROM Ticket_Bet WHERE ticketID = ?";
    $stmtTicketBet = $pdoConn->prepare($sqlTicketBet);
    $stmtTicketBet->execute(array($ticketID));
    $TicketBets = $stmtTicketBet->fetchAll(PDO::FETCH_ASSOC);

    $sqlTicketBerPart = "SELECT * FROM Ticket_Bet_Part WHERE ticketBetID = ?";
    $stmtTicketBetPart = $pdoConn->prepare($sqlTicketBerPart);
    
}catch (Exception $e){
    echo 'A ERROR HAS OCURRED';
}




?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Ticket</title>
</head>
<body onload="window.print();" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" style="height: 100%; width: 100%">
<div id="Factura"
     style="width:250px; font-family:'Times New Roman', Times, serif; font-size:16px; text-transform:uppercase;">

    <div align="center"><a href="#" onclick="window.print()">PRINT</a></div>

    <div align="center"> QUATRO <br/>
        <hr/>
        USER: <?php echo $_SESSION['NombreUsuario']; ?><br/></div>
    <hr/>
    <table width="100%">
        <tr>
            <td>Ticket:</td>
            <td style="font-size:20px; font-weight: bold;" ># <?php echo $ticketID; ?></td>
        </tr>
        <tr>
            <td>Date:</td>
            <td> <?php echo $ticket['created_at']; ?></td>
        </tr>
        <tr>
            <td>Total:</td>
            <td style="font-size:20px; font-weight: bold;" > <?php echo $ticket['total']; ?></td>
        </tr>
        <tr>
            <td>Draw:</td>
            <td style="font-size:20px; font-weight: bold;" > <?php echo system_date_format($sorteo['FechayHora']); ?></td>
        </tr>
    </table>
    <hr/>
    <table width="100%" style="text-align: center; border-spacing: 2px">

        <tr>
            <td>1st</td>
            <td>2nd</td>
            <td>3rd</td>
            <td>4th</td>
            <td>Amount</td>
        </tr>
        <colgroup span="4" style="border: 1px solid black;">
        </colgroup>
        <?php foreach ($TicketBets as $bet): ?>
            <?php $stmtTicketBetPart->execute(array($bet['id'])); ?>
            <?php $betParts = $stmtTicketBetPart->fetchAll(PDO::FETCH_ASSOC); ?>
            <tr>
               <?php if($bet['scoring_type'] == 1){ ?>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" >-</td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" >-</td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" >-</td>
                   <td style="font-size:24px; font-weight: bold; text-align: center; border: 1px solid black;" ><?php echo $bet['amount']; ?> </td>
               <?php }elseif($bet['scoring_type'] == 2){ ?>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" >-</td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" >-</td>
                   <td style="font-size:24px; font-weight: bold; text-align: center; border: 1px solid black;" ><?php echo $bet['amount']; ?> </td>
               <?php }elseif($bet['scoring_type'] == 3){ ?>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[2]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" >-</td>
                   <td style="font-size:24px; font-weight: bold; text-align: center; border: 1px solid black;" ><?php echo $bet['amount']; ?> </td>
               <?php }elseif($bet['scoring_type'] == 4){ ?>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[2]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[3]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center; border: 1px solid black;" ><?php echo $bet['amount']; ?> </td>
               <?php }elseif($bet['scoring_type'] == 5){ ?>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[2]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center" ><?php echo $betParts[3]['number']; ?> </td>
                   <td style="font-size:24px; font-weight: bold; text-align: center; border: 1px solid black;" ><?php echo $bet['amount']; ?>AO </td>
               <?php } ?>

            </tr>

        <?php
        endforeach;
        ?>



    </table>
    <table width="100%" style="text-align: center">
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="8" text-align="center">*Thank you*</td>
        </tr>
    </table>


</div>

</body>




