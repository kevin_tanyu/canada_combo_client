<?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$action = $_POST['action'];

try{
    if($action == 'Check'){

        $nowDateTime = date('Y-m-d H:i:s');
        $sqlGetDraw = "SELECT * FROM SorteosProgramacion
                       WHERE FechayHora > ?";
        $stmtGetDraw = $pdoConn->prepare($sqlGetDraw);
        //$nowDateTime = date('Y-m-d H:i:s', strtotime($nowDateTime . ' + 10 minutes'));
        $stmtGetDraw->execute(array($nowDateTime));
        $dailyDraw = $stmtGetDraw->fetch();

        echo $dailyDraw['ID'];


    }//FIN check
    elseif($action == 'Save'){

        /*GET POST VALUES*/
        $combinationsTicket = $_POST['combinations'];
        $scoringCombination = $_POST['scoring'];
        $sorteoID = $_POST['SorteoID'];
        $total = $_POST['total'];
        /*HORA DEL SISTEMA*/
        $nowDateTime = date('Y-m-d H:i:s');

        /*SQL AGREGAR TICKET*/
        $sqlAddTicket = "INSERT INTO Ticket(sorteoID, usuarioID, total, created_at)
                         VALUES(:sorteoID, :usuarioID, :total, :created_at)";
        $stmtAddTicket = $pdoConn->prepare($sqlAddTicket);
        $stmtAddTicket->execute(array(':sorteoID' => $sorteoID, ':usuarioID' => $_SESSION['IDUsuario'], ':total' => $total, ':created_at' => $nowDateTime));

        /*GET ID INGRESADO*/
        $lastTicketID = $pdoConn->lastInsertId();

        /*SQL ADD COMBINACIONES AL TICKET*/
        $sqlAddTicketBet = "INSERT INTO Ticket_Bet(ticketID, amount, scoring_type)
                            VALUES(:ticketID, :amount, :scoring)";
        $stmtAddTicketBet = $pdoConn->prepare($sqlAddTicketBet);

        /*SQL ADD COMBINATION DETAIL*/
        $sqlAddTicketBetPart = "INSERT INTO Ticket_Bet_Part(ticketBetID, number, orderNumber)
                                VALUES(:ticketBetID, :number, :numberOrder)";
        $stmtAddTicketBetPart = $pdoConn->prepare($sqlAddTicketBetPart);

        for($i = 0; $i < sizeof($combinationsTicket); $i++){
            $combinationTemp = $combinationsTicket[$i];//ULTIMO CAMPO DEL ARRAY ES EL MONTO DE LA COMBINACION

            $stmtAddTicketBet->execute(array(':ticketID' => $lastTicketID, ':amount' => $combinationTemp[sizeof($combinationTemp)-1], ':scoring' => $scoringCombination[$i]));

            /*GET ID INGRESADO*/
            $lastTicketBetID = $pdoConn->lastInsertId();
            for($m = 0; $m < sizeof($combinationTemp)-1; $m++){
                $stmtAddTicketBetPart->execute(array(':ticketBetID' => $lastTicketBetID,':number' => $combinationTemp[$m],':numberOrder' => $m+1));
            }//FIN FOR BET PART

        }//FIN FOR BET

        echo $lastTicketID;

    }//FIN save
    elseif($action == 'Payment'){

        $username = $_SESSION['NombreUsuario'];
        $ticketID = $_POST['TicketID'];
        $ticketBetPart = $_POST['TicketBetPart'];
        $prize = $_POST['Prize'];
        $date = date('Y-m-d H:i:s');
        $userID = $_SESSION['IDUsuario'];

        $sqlInsertPayment = "INSERT INTO Ticket_Payment(ticketID, ticketBetID, prize, pay_at, pay_by)
                              VALUES(?,?,?,?,?);";
        $stmtInsertPayment = $pdoConn->prepare($sqlInsertPayment);
        for($i = 0; $i < sizeof($ticketBetPart); $i++){
            $stmtInsertPayment->execute(array($ticketID, $ticketBetPart[$i], $prize[$i], $date, $userID));
        }

        /**********INFO DEL TIQUETE PAGADO*********/
        $sqlComprobarPago = "SELECT * FROM Ticket_Payment TP
                             JOIN Usuarios U ON TP.pay_by = U.ID
                             WHERE TP.ticketID = ?";
        $stmtComprobarPago = $pdoConn->prepare($sqlComprobarPago);
        $stmtComprobarPago->execute(array($ticketID));
        $pagado = $stmtComprobarPago->fetch();

        echo "<label style='font-size: 25px'>Pay by <?php echo '<span style='font-weight: bold'>" . $pagado['NombreUsuario'] . "</span> at " .  system_date_format($pagado['pay_at']) . "</label>";

    }//Fin payment
    elseif($action == 'Combination'){

        $sorteoID = $_POST['SorteoID'];
        $pick = $_POST['Pick'];
        $Monto = $_POST['Monto'];


        /*LIMITES DE APUESTAS*/
        $sqlLimitParam = "SELECT *
                          FROM Scoring_Limits
                          WHERE scoring_type = ?";
        $stmtLimitParam = $pdoConn->prepare($sqlLimitParam);
        $stmtLimitParam->execute(array($pick));
        $limitParam = $stmtLimitParam->fetch();

        /*******GET VENTA********/
        $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE sorteoID = ?";
        $stmtTiquetes = $pdoConn->prepare($sqlTiquetes);
        $stmtTiquetes->execute(array($sorteoID));
        $Tiquetes = $stmtTiquetes->fetchAll(PDO::FETCH_ASSOC);

        /************QUERY BET_POR_TICKET***************/
        $sqlApuestas = "SELECT *
                        FROM  Ticket_Bet
                        WHERE  ticketID  = ? AND scoring_type = ? ";
        $stmtApuestas = $pdoConn->prepare($sqlApuestas);

        $sqlApuestasMixed = "SELECT *
                             FROM  Ticket_Bet
                             WHERE  (ticketID  = ? AND scoring_type = ?) OR (ticketID  = ? AND scoring_type = ?)";
        $stmtApuestasMixed = $pdoConn->prepare($sqlApuestasMixed);

        /************QUERY COMBINACION BET_POR_TICKET***************/
        $sqlCombinaciones = "SELECT *
                             FROM Ticket_Bet_Part
                             WHERE ticketBetID = ?
                             ORDER BY orderNumber ASC";
        $stmtCombinaciones = $pdoConn->prepare($sqlCombinaciones);

        $totalCom = 0;//LLEVAR EL MONTO TOTAL DE LA COMBINACION

        foreach($Tiquetes as $tiquete){
                $stmtApuestas->execute(array($tiquete['id'], $pick));
                $Apuestas = $stmtApuestas->fetchAll(PDO::FETCH_ASSOC);


            foreach($Apuestas as $apuesta){
                $stmtCombinaciones->execute(array($apuesta['id']));
                $Combinaciones = $stmtCombinaciones->fetchAll(PDO::FETCH_ASSOC);

                $combinacionTemp = array();
                foreach($Combinaciones as $combinacion){
                    $numero = $combinacion['number'];
                    array_push($combinacionTemp, $numero);
                }//FIN FOREACH $Combinaciones

                if($pick == 1){

                    $Numero1 = $_POST['Numero1'];

                    if($Numero1 == $combinacionTemp[0]){
                        $totalCom = $totalCom + $apuesta['amount'];
                    }//Fin if

                }else if($pick == 2){

                    $Numero1 = $_POST['Numero1'];
                    $Numero2 = $_POST['Numero2'];;

                    if($Numero1 == $combinacionTemp[0] && $Numero2 == $combinacionTemp[1]){
                        $totalCom = $totalCom + $apuesta['amount'];
                    }//Fin if

                }else if($pick == 3){

                    $Numero1 = $_POST['Numero1'];
                    $Numero2 = $_POST['Numero2'];
                    $Numero3 = $_POST['Numero3'];

                    if($Numero1 == $combinacionTemp[0] && $Numero2 == $combinacionTemp[1] && $Numero3 == $combinacionTemp[2]){
                        $totalCom = $totalCom + $apuesta['amount'];
                    }//Fin if

                }else if($pick == 4){

                    $Numero1 = $_POST['Numero1'];
                    $Numero2 = $_POST['Numero2'];
                    $Numero3 = $_POST['Numero3'];
                    $Numero4 = $_POST['Numero4'];

                    if($Numero1 == $combinacionTemp[0] && $Numero2 == $combinacionTemp[1] && $Numero3 == $combinacionTemp[2] && $Numero4 == $combinacionTemp[3]){
                        $totalCom = $totalCom + $apuesta['amount'];
                    }//Fin if

                }else if($pick == 5){

                    $Numero1 = $_POST['Numero1'];
                    $Numero2 = $_POST['Numero2'];
                    $Numero3 = $_POST['Numero3'];
                    $Numero4 = $_POST['Numero4'];

                    $arrayNumeros = array();
                    array_push($arrayNumeros, $Numero1);
                    array_push($arrayNumeros, $Numero2);
                    array_push($arrayNumeros, $Numero3);
                    array_push($arrayNumeros, $Numero4);

                    $hits = 0;

                    for($i = 0; $i < sizeof($combinacionTemp); $i++){

                        for($j = 0; $j < sizeof($arrayNumeros); $j++){

                            if($combinacionTemp[$i] == $arrayNumeros[$j]){
                                $hits++;
                                $arrayNumeros[$j] = -1;
                            }//Fin if

                        }//Fin for

                    }//Fin for

                    if($hits == 4){
                        $totalCom = $totalCom + $apuesta['amount'];
                    }//Fin if hits

                }//FIN ELSE IF


            }//FIN FOREACH $Apuestas


        }//FIN FOREACH $Tiquetes

        if(($Monto + $totalCom) > $limitParam['maxi']){
            echo "Combination amount limit exceed in $" . (($Monto + $totalCom) - $limitParam['maxi']);
        }else{
            if(($Monto + $totalCom) < $limitParam['mini']){
                echo "Minimun bet combination is $" . $limitParam['mini'];
            }else{
                echo 'TRUE';
            }
        }

    }//Fin combination


      /* elseif($action == 'Combination'){

           $sorteoID = $_POST['SorteoID'];
           $Numero1 = $_POST['Numero1'];
           $Numero2 = $_POST['Numero2'];
           $Numero3 = $_POST['Numero3'];
           $Numero4 = $_POST['Numero4'];
           $Monto = $_POST['Monto'];*/


        /*PARAMETROS DE APUESTAS*/
       /* $sqlBetParam = "SELECT * FROM Bet_Parameters";
        $stmtBetParam = $pdoConn->prepare($sqlBetParam);
        $stmtBetParam->execute();
        $betParam = $stmtBetParam->fetch();*/

        /*******GET VENTA********/
       /* $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE sorteoID = ?";


        $stmtTiquetes = $pdoConn->prepare($sqlTiquetes);
        $stmtTiquetes->execute(array($sorteoID));
        $Tiquetes = $stmtTiquetes->fetchAll(PDO::FETCH_ASSOC);*/

        /************QUERY BET_POR_TICKET***************/
       /* $sqlApuestas = "SELECT *
                    FROM  Ticket_Bet
                    WHERE  ticketID  = :ticket_id";
        $stmtApuestas = $pdoConn->prepare($sqlApuestas);*/
        /**********************************************/

        /************QUERY COMBINACION BET_POR_TICKET***************/
      /*  $sqlCombinaciones = "SELECT *
                               FROM Ticket_Bet_Part
                               WHERE ticketBetID = :apuesta_id
                               ORDER BY orderNumber ASC";
        $stmtCombinaciones = $pdoConn->prepare($sqlCombinaciones);*/
        /**********************************************/

      /*  $totalCom = 0;

        foreach($Tiquetes as $tiquete){
            $stmtApuestas->execute(array(':ticket_id' => $tiquete['id']));
            $Apuestas = $stmtApuestas->fetchAll(PDO::FETCH_ASSOC);

            foreach($Apuestas as $apuesta){
                $stmtCombinaciones->execute(array(':apuesta_id' => $apuesta['id']));
                $Combinaciones = $stmtCombinaciones->fetchAll(PDO::FETCH_ASSOC);

                $combinacionTemp = array();
                foreach($Combinaciones as $combinacion){
                    $numero = $combinacion['number'];
                    array_push($combinacionTemp, $numero);
                }//FIN FOREACH $Combinaciones

                if($Numero1 == $combinacionTemp[0] && $Numero2 == $combinacionTemp[1] && $Numero3 == $combinacionTemp[2] && $Numero4 == $combinacionTemp[3]){
                    $totalCom = $totalCom + $apuesta['amount'];
                }//Fin if

            }//FIN FOREACH $Apuestas


        }//FIN FOREACH $Tiquetes


        if(($Monto + $totalCom) > $betParam['maxi']){
            echo "Combination amount limit exceed in $" . (($Monto + $totalCom) - $betParam['maxi']);
        }else{
            if(($Monto + $totalCom) < $betParam['mini']){
                echo "Minimun bet combination is $" . $betParam['mini'] . "/" . $totalCom;
            }else{
                echo 'TRUE';
            }
        }


    }//Fin Combination*/

}catch(Exception $e){
    echo 'ERROR';
}

?>