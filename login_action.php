<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 10:13 AM
 */

session_start();

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

include("config.ini.php");
include("conectadb.php");


$sqlLogin = "SELECT * FROM Usuarios WHERE NombreUsuario = :username and Contrasena = :password and ActivoFlag = 1 and NivelUsuario = 4";
$stmtLogin = $pdoConn->prepare($sqlLogin);
$stmtLogin->execute(array(':username' => $_POST['txtusername'],':password' => $_POST['txtpassword']));



if ($stmtLogin->rowCount() > 0)
{
    // Encontro coincidencia de User y Pass


    // Inicia Sesiones

    $row = $stmtLogin->fetch();

    if ($row['ActivoFlag'] == 1)
    { // El Usuario esta Activo

        $_SESSION['IDUsuario']      = $row['ID'];
        $_SESSION['NombreUsuario']  = $row['NombreUsuario'];


        /*******************NuevaEstructuraPermisos******************************/
        $_SESSION['NivelUsuario'] = $row['NivelUsuario'];
        switch ($row['TipoUsuarioNivel'])
        {
            case 1:
                $_SESSION['TipoUsuarioNivel'] = "House";
                break;
            case 2:
                $_SESSION['TipoUsuarioNivel'] = "Agent";
                break;
            case 3:
                $_SESSION['TipoUsuarioNivel'] = "Store";
                break;
            case 4:
                $_SESSION['TipoUsuarioNivel'] = "Jugador";
                break;
            case 5:
                $_SESSION['TipoUsuarioNivel'] = "Bank";
                break;
        }


        /*******************NuevaEstructuraPermisos******************************/

        $_SESSION['ActivoFlag'] = $row['ActivoFlag'];

        if ($row['IDPadre'] <> "")
        {
            $QUERY                     = "SELECT NombreUsuario FROM Usuarios WHERE ID=" . $row['IDPadre'];
            $rs2                       = mysql_query($QUERY);
            $row2                      = mysql_fetch_row($rs2);
            $_SESSION['IDPadreNombre'] = $row2[0];
            $_SESSION['IDPadre']       = $row['IDPadre'];
        }

        $_SESSION['Email'] = $row['Email'];



        header("Location: home.php");


        exit;
    } else
    {
        header("Location: login.php?IDM=" . $row['ActivoFlag']);
        exit;
    }
} else
{
    header("Location: login.php?IDM");
    exit;
}

